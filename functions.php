<?php

/**

 * 

 * Function file

 *

 * @package   Playbox_Youtube

 * @author    7Span

 * @copyright 2024 7Span

 * @license   GPL-2.0-or-later

 */



if (!defined("ABSPATH")) {

	exit();

}

require_once PLAYBOX_YOUTUBE_PLUGIN_DIR . 'includes/class-playbox-youtube.php';





/**

 * Register or init widget to display on front end.

 * @return boolean valid api check.

 */

// phpcs:ignore

function api_validate() {

	$api_key = get_option( 'youtube_api_id' );

	if ( empty( $api_key ) ) {

		return false;

	} else {

		return true;

	}

}



/**

 * Shortcode Playbox of Yuoutube.

 */

// phpcs:ignore

function playbox_youtube_embeed($atts, $content = null) {

	if ( api_validate() === false ) {

		$output_string = esc_html__( 'Enter YouTube API Key', 'playbox-of-youtube-by-7span' );

		return $output_string;

	}



	$feed_attr = shortcode_atts(

		array(

			'title' => true,

			'playlist-id'      => '',

			'number-of-videos' => 6,

			'playlist-views'   => 'grid',

			'channel-name'     => true,

			'publish-date'     => true,

		),

		$atts

	);

	if ( $feed_attr['playlist-id'] ) {

		global $video_items;

		$api_key = get_option( 'youtube_api_id' );

		playbox_youtube_embeed_item(

			$feed_attr['playlist-id'],

			$feed_attr['number-of-videos'],

			$api_key

        ); // phpcs:ignore

		if ( empty( $video_items ) ) {

			$output_string = esc_html__(

				'Incorrect playlist ID',

				'playbox-of-youtube-by-7span'

			);

			return $output_string;

		} else {

			ob_start();

			$view_data = array();

			$view_data = array(

				'view_style'        => $feed_attr['playlist-views'],

				'channel_view_meta' => $feed_attr['channel-name'],

				'publish_view_meta' => $feed_attr['publish-date'],

				'title_view' =>  $feed_attr['title']

			);

			playbox_youtube_template_part(

				'includes/embed',

				'playbox-youtube',

				$view_data

            ); // phpcs:ignore

			$output_string = ob_get_contents();

			ob_end_clean();



			return $output_string;

		}

	} else {

		$output_string = esc_html__(

			'Please enter playlist ID',

			'playbox-of-youtube-by-7span'

		);

		return $output_string;

	}

}

add_shortcode( 'playbox_youtube_list', 'playbox_youtube_embeed' );



/**

 * @param string $youtube_playlist_id playlist id.

 * @param null $name current template name.

 * @param int $number_of_videos give number of video.

 * @param string $api_key youtube API key.

 * @return array video item.

 * Output Items

 */

// phpcs:ignore

function playbox_youtube_embeed_item( $youtube_playlist_id, $number_of_videos, $api_key) {

	global $video_items;

	$part        = 'snippet,contentDetails';

	$playlist_id = $youtube_playlist_id;

	$max_results = $number_of_videos;



	/* by playlist */

	$feedurl = 'https://www.googleapis.com/youtube/v3/playlistItems?part=' . $part . '&playlistId=' . $playlist_id . '&maxResults=' . $max_results . '&key=' . $api_key;



	$feed = getSslPage( $feedurl );



	$feed_arr = json_decode( $feed, true );

	// phpcs:ignore

	if ( $feed_arr === null ) {

		$video_items = '';

	} elseif ( array_key_exists( 'error', $feed_arr ) ) {

		$video_items = '';

	} else {

		$video_items = $feed_arr['items'];

	}



	return $video_items;

}



/**

 * Get you tube video url

 * @param string $url video url.

 * @return string content video.

 */

// phpcs:ignore

function getSslPage($url) {

	$result = wp_remote_retrieve_body( wp_remote_get( $url ) );

	return $result;

}



/**

 * Own custom part template.

 * @param string $slug current template slug.

 * @param null $name current template name.

 * @param array $args give any argument to current template.

 * @return string current template url.

 */

// phpcs:ignore

function playbox_youtube_template_part($slug, $name = null, $args = []) {

    // phpcs:ignore

    if (is_array($args) && isset($args)):

    	// phpcs:ignore

		extract( $args );

	endif;



	do_action( "playbox_youtube_template_part{$slug}", $slug, $name, $args );



	$templates = array();

	if ( isset( $name ) ) {

		$templates[] = "{$slug}-{$name}.php";

	}



	$templates[] = "{$slug}.php";



	feeder_get_template_path( $templates, $args, true, false );

}



/*

Extend locate_template from WP Core

* Define a location of your plugin file dir to a constant in this case = PLUGIN_DIR_PATH

* Note: PLAYBOX_YOUTUBE_PLUGIN_DIR - can be any folder/subdirectory within your plugin files

*/

/**

 * @param array $template_names current template name.

 * @param array $args give any argument to current template.

 * @param boolean $load give default false.

 * @param boolean $require_once give default true.

 * @return string current template url.

 */

// phpcs:ignore

function feeder_get_template_path( $template_names,$args,$load = false,$require_once = true ) {

	$located = '';

	foreach ( (array) $template_names as $template_name ) {

		if ( ! $template_name ) {

			continue;

		}



		/* search file within the PLUGIN_DIR_PATH only */

		if ( file_exists( PLAYBOX_YOUTUBE_PLUGIN_DIR . $template_name ) ) {

			$located = PLAYBOX_YOUTUBE_PLUGIN_DIR . $template_name;

			break;

		}

	}

	if ( $load && '' !== $located ) {

		load_template( $located, $require_once, $args );

	}

	return $located;

}



// phpcs:ignore

/**

 * Register block render function call

 * @param string $video_feeder_publish current video date time.

 * @return string video date time.

 */

function video_feeder_time($video_feeder_publish) {

	$datetime_1 = gmdate( 'Y-m-d h:i:s', strtotime( $video_feeder_publish ) );

	$datetime_2 = gmdate( 'Y-m-d h:i:s' );



	$start_datetime = new DateTime( $datetime_1 );

	$diff           = $start_datetime->diff( new DateTime( $datetime_2 ) );

	if ( $diff->y > 0 ) {

		$public_video = $diff->y . ' years ago';

	} elseif ( $diff->m > 0 ) {

		$public_video = $diff->m . ' months ago';

	} elseif ( $diff->d > 0 ) {

		$public_video = $diff->d . ' days ago';

	} elseif ( $diff->h > 0 ) {

		$public_video = $diff->h . ' hours ago';

	} elseif ( $diff->i > 0 ) {

		$public_video = $diff->i . ' minutes ago';

	} else {

		$public_video = $diff->s . ' seconds ago';

	}

	return $public_video;

}





/**

 * Register block

 */

add_action( 'init', 'register_playbox_youtube_block' );

// phpcs:ignore

function register_playbox_youtube_block() {

    // phpcs:ignore

    register_block_type("playbox-youtube/youtube-cta", [

		'editor_script'   => 'custom-gutenburg-js',

		'attributes'      => array(

			'playlistid'      => array(

				'type' => 'string',

			),

			'numbervideos'    => array(

				'type'    => 'integer',

				'default' => 3,

			),

			'playlistviews'   => array(

				'type'    => 'string',

				'default' => 'grid',

			),

			'channelviewmeta' => array(

				'type'    => 'boolean',

				'dafault' => false,

			),

			'publishviewmeta' => array(

				'type'    => 'boolean',

				'dafault' => false,

			),

			'title_view' => array(

				'type'    => 'boolean',

				'dafault' => true,

			),

		),

		'render_callback' => 'gutenberg_playbox_youtube_render_callback',

        // phpcs:ignore

    ]);

}



/**

 * Register block render function call

 * @param array $block_attributes block attribute.

 * @param string $content.

 * @return string embed content.

 */

// phpcs:ignore

function gutenberg_playbox_youtube_render_callback($block_attributes, $content) {

	if ( api_validate() === false ) {

		$output_string = esc_html__( 'Enter YouTube API Key', 'playbox-of-youtube-by-7span' );

	} else {

        // phpcs:ignore


		$playlist_id     = isset( $block_attributes['playlistid'] )

			? $block_attributes['playlistid']

			: '';

		$numbervideos    = isset( $block_attributes['numbervideos'] )

			? $block_attributes['numbervideos']

			: 0;

		$playlistviews   = $block_attributes['playlistviews'];

		$channelviewmeta = isset( $block_attributes['channelviewmeta'] )

			? $block_attributes['channelviewmeta']

			: false;

		$publishviewmeta = isset( $block_attributes['publishviewmeta'] )

			? $block_attributes['publishviewmeta']

			: false;



		$title_view = isset( $block_attributes['title_view'] )

		? $block_attributes['title_view']

		: true;



		$channelviewmeta = filter_var(

			$channelviewmeta,

			FILTER_VALIDATE_BOOLEAN,

			FILTER_NULL_ON_FAILURE

		);

		$publishviewmeta = filter_var(

			$publishviewmeta,

			FILTER_VALIDATE_BOOLEAN,

			FILTER_NULL_ON_FAILURE

		);



		$title_view = filter_var(

			$title_view,

			FILTER_VALIDATE_BOOLEAN,

			FILTER_NULL_ON_FAILURE

		);



		if ( ! empty( $playlist_id ) ) {

			global $video_items;

			$api_key = get_option( 'youtube_api_id' );

			playbox_youtube_embeed_item( $playlist_id, $numbervideos, $api_key );

			if ( empty( $video_items ) ) {

				$output_string = esc_html__(

					'Incorrect playlist ID',

					'playbox-of-youtube-by-7span'

				);

			} else {

				ob_start();

				$view_data = array();

				$view_data = array(

					'view_style'        => $playlistviews,

					'channel_view_meta' => $channelviewmeta,

					'publish_view_meta' => $publishviewmeta,

					'title_view' => $title_view,

				);

				playbox_youtube_template_part(

					'includes/embed',

					'playbox-youtube',

					$view_data

				);

				$output_string = ob_get_contents();

				ob_end_clean();

			}

		} else {

			$output_string = esc_html__(

				'Please enter playlist ID',

				'playbox-of-youtube-by-7span'

			);

		}

	}

	wp_reset_postdata();

	return $output_string;

}

