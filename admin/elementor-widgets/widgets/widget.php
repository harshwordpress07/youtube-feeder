<?php //phpcs:ignore

/**

 * Playbox Youtube Widget Class

 */
class Playbox_Youtube_Widget extends \Elementor\Widget_Base

{

    public function __construct($data = [], $args = null)// phpcs:ignore

    {

        parent::__construct($data, $args);

        if (defined('PLAYBOX_YOUTUBE_VERSION') ) {

            $this->version = PLAYBOX_YOUTUBE_VERSION;

        } else {

            $this->version = '1.0.0';

        }

        $this->plugin_name = 'post-type-listing';

        

        wp_register_script('magnific-popup-js', PLAYBOX_YOUTUBE_PLUGIN_URL. 'assets/js/jquery.magnific-popup.js', array( 'jquery' ), $this->version, true);

        wp_register_script('playbox-youtube-player-api', 'https://www.youtube.com/player_api', array( 'jquery' ), $this->version, true);

        wp_register_script('playbox-youtube-js', PLAYBOX_YOUTUBE_PLUGIN_URL.'assets/js/playbox-youtube.js', array( 'jquery' ), $this->version, true);

    }





    /**

     * Get widget name.

     *

     * Retrieve Playbox Youtube widget name.

     *

     * @since  1.0.0

     * @access public

     *

     * @return string Widget name.

     */

    public function get_name()

    {

        return 'youtubefeed';

    }





    /**

     *

     * JS dependency on Playbox of Youtube.

     *

     * @since  1.0.0

     * @access public

     *

     * @return string Widget name.

     */

    public function get_script_depends()

    {    

        return [ 'magnific-popup-js', 'playbox-youtube-player-api','playbox-youtube-js' ];

    }





    /**

     * Get widget title.

     *

     * Retrieve Playbox of Youtube widget title.

     *

     * @since  1.0.0

     * @access public

     *

     * @return string Widget title.

     */

    public function get_title()

    {

        return __('YouTube Playbox', 'playbox-of-youtube-by-7span');

    }



    /**

     * Get widget icon.

     *

     * Retrieve Playbox of Youtube widget icon.

     *

     * @since  1.0.0

     * @access public

     *

     * @return string Widget icon.

     */

    public function get_icon()

    {

        return 'eicon-video-playlist';

    }



    /**

     * Get widget categories.

     *

     * Retrieve the list of categories the Playbox of Youtube widget belongs to.

     *

     * @since  1.0.0

     * @access public

     *

     * @return array Widget categories.

     */

    public function get_categories()

    {

        return [ 'general' ];

    }



    /**

     * Register Playbox of Youtube widget controls.

     *

     * Adds different input fields to allow the user to change and customize the widget settings.

     *

     * @since  1.0.0

     * @access protected

     */

    protected function register_controls()

    {



        $this->start_controls_section(

            'content_section',

            [

            'label' => esc_html__('YouTube Playbox', 'playbox-of-youtube-by-7span'),

            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,

            ]

        );



        $this->add_control(

            'playlist_id',

            [

            'label' => esc_html__('Playlist ID', 'playbox-of-youtube-by-7span'),

            'type' => \Elementor\Controls_Manager::TEXT,

            'placeholder' => esc_html__('YouTube playlist ID *', 'playbox-of-youtube-by-7span'),

            ]

        );



        $this->add_control(

            'number_of_videos',

            [

            'type' => \Elementor\Controls_Manager::NUMBER,

            'label' => esc_html__('Number Of Videos', 'playbox-of-youtube-by-7span'),

            'placeholder' => '0',

            'min' => 1,

            'max' => 100,

            'step' => 1,

            'default' => 6,

            ]

        );



        

        $this->end_controls_section();



        $this->start_controls_section(

            'style_section',

            [

            'label' => esc_html__('Playlist Style', 'playbox-of-youtube-by-7span'),

            'tab' => \Elementor\Controls_Manager::TAB_STYLE,

            ]

        );



        $this->add_control(

            'view_style',

            [

            'label' => esc_html__('Select Style', 'playbox-of-youtube-by-7span'),

            'type' => \Elementor\Controls_Manager::SELECT,

            'default' => 'grid',

            'options' => [

            'grid' => esc_html__('Grid', 'playbox-of-youtube-by-7span'),

            'list' => esc_html__('List', 'playbox-of-youtube-by-7span'),

            ],

            ]

        );



        $this->add_control(

            'title_view',

            [

                'label' => esc_html__('Title View', 'playbox-of-youtube-by-7span'),

                'type' => \Elementor\Controls_Manager::SWITCHER,

                'label_on' => esc_html__('Show', 'playbox-of-youtube-by-7span'),

                'label_off' => esc_html__('Hide', 'playbox-of-youtube-by-7span'),

                'return_value' => 'yes',

                'default' => 'yes',

            ]

        );



        $this->end_controls_section();



        $this->start_controls_section(

            'meta_section',

            [

            'label' => esc_html__('Video Meta', 'playbox-of-youtube-by-7span'),

            'tab' => \Elementor\Controls_Manager::TAB_STYLE,

            ]

        );



        $this->add_control(

            'channel_view_meta',

            [

            'label' => esc_html__('Channel Name', 'playbox-of-youtube-by-7span'),

            'type' => \Elementor\Controls_Manager::SWITCHER,

            'label_on' => esc_html__('Show', 'playbox-of-youtube-by-7span'),

            'label_off' => esc_html__('Hide', 'playbox-of-youtube-by-7span'),

            'return_value' => 'yes',

            'default' => 'yes',

            ]

        );



        $this->add_control(

            'publish_view_meta',

            [

            'label' => esc_html__('Publish Date', 'playbox-of-youtube-by-7span'),

            'type' => \Elementor\Controls_Manager::SWITCHER,

            'label_on' => esc_html__('Show', 'playbox-of-youtube-by-7span'),

            'label_off' => esc_html__('Hide', 'playbox-of-youtube-by-7span'),

            'return_value' => 'yes',

            'default' => 'yes',

            ]

        );

        $this->end_controls_section();

    }



    /**

     * Render Playbox of Youtube widget output on the frontend.

     *

     * Written in PHP and used to generate the final HTML.

     *

     * @since  1.0.0

     * @access protected

     */

    protected function render()

    {

        if(api_validate() == false ) {

            echo esc_html_e('Enter YouTube API Key', 'playbox-of-youtube-by-7span');

        }else{

            $settings = $this->get_settings_for_display(); 

            if($settings['playlist_id'] ) {

                global $video_items; 

                $api_key = get_option('youtube_api_id');

                playbox_youtube_embeed_item($settings['playlist_id'], $settings['number_of_videos'], $api_key);

                if(empty($video_items)  ) {

                    echo esc_html_e('Incorrect playlist ID', 'playbox-of-youtube-by-7span');
                    
                }else{ ?>

                    <script>

                        tigger_feeder_editor();

                    </script>

                    <?php ob_start();

                    $view_data = [];

                    $view_data = [ 'view_style'=> $settings['view_style'] , 'channel_view_meta'=> $settings['channel_view_meta'],'publish_view_meta'=> $settings['publish_view_meta'],'title_view' => $settings['title_view'] ];

                
                    playbox_youtube_template_part('includes/embed', 'playbox-youtube', $view_data);

                    $output_string = ob_get_contents();

                    ob_end_clean();

                    echo wp_kses_post($output_string);

                }

            }

            else{

                echo  esc_html_e('Please enter playlist ID', 'playbox-of-youtube-by-7span');

            }

        }

        

    }

}

