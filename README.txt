=== Playbox of Youtube By 7Span ===
Contributors: Harsh Maru, YashShah, harpalsinhchauhan
Plugin Name: Playbox of Youtube By 7Span
Plugin URI: https://wordpress.org/plugins/playbox-youtube-by-7span
Author: 7Span
Stable Tag: 1.0.0
Author URI: https://7span.com
Tags: youtube playlist,youtube feeds,youtube playlist tips,youtube rss feed,playlist feed.
Tested up to: 6.5.3
License: GPLv2 

A plugin for embedding and auto-updating YouTube playlists with customizable layout on your website.

== Description ==

A plugin to embed and auto-update YouTube playlists on your website, with customizable layout.

**Plugin Features**

- Playbox of Youtube supports the functionality to share website blogs on above supported social platforms.
- Playbox of Youtube supports the functionality to follow Website on above supported social platforms.
- Playbox of Youtube supports the functionality to follow Blog or Website Author on above supported social platforms.
- 100% responsive. 
- No Image, button created using CSS3. For device width less than 480px, this plugin used one single image to display social icons.
- Provide Shortcode to add the social share buttons to share website blogs on above social platforms.
- Provide Shortcode for Website Playbox of YouTube to follow Website on above social platforms.
- Provide Shortcode for Author Playbox YouTube to follow Blog Author on above social platforms. 
- These shortcode can integrated with any theme easily.
- These social buttons can be add anywhere in the posts.

**Plugin Shortcode**
You can use following shortcodes

- Shortcode for Playbox of Youtube
  - Shortcode parameter : playlist-id , number-of-videos, title, playlist-views, channel-name, publish-date 
  - To display videos list :
    [playbox_youtube_list playlist-id='{playlist_id}' playlist-views='{grid/list}' channel-name={true/false} title={true/false} number-of-videos={number} publish-date={true/false}]

== Screenshots ==

1. Website Social Share buttons 
2. Website Playbox of Youtube Settings in Admin Panel
3. Website Social Links and Counters Settings in Admin Panel
4. Website Playbox of YouTube buttons in UI
5. Author Playbox of Youtube Settings in Admin Panel
6. Author Playbox of Youtube Buttons with Author information
7. Author Playbox of Youtube Buttons with Author information when width less than 480px

== Installation ==

= Installing the plugin =

1. In your WordPress admin panel, go to *Plugins > New Plugin*, search for *Plybox of YouTube By 7Span* and click "Install now"
2. Alternatively, download the plugin and upload the contents of `playbox-of-youtube-by-7span.php` to your plugins directory, which usually is `/wp-content/plugins/`. Activate the plugin.


== Frequently Asked Questions ==

= Is Playbox of YouTube free? =

The Playbox of YouTube is 100% free.

= What does Playbox of Youtube let me do? =

Playbox of Youtube adds very attractive responsive social icons to share blogs and social follow buttons to follow website and blog author with their likes or followers counters of all supported social platforms mentioned above.

= Do I have to install any software on my server to get this working? =

Not at all! Social Share is a hosted social share service. Simply configure the plugin and you're done!

== Change log ==

= 1.0 - May 27, 2024 =
* First release.