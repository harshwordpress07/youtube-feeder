const { registerBlockType } = wp.blocks;

const { RichText, InspectorControls,ServerSideRender } = wp.editor;

const { PanelBody, TextControl, SelectControl, ToggleControl } = wp.components;



registerBlockType('playbox-youtube/youtube-cta', {

  title: 'YouTube Playbox',

  icon: 'video-alt3',

  category: 'common',

  attributes: {

    numbervideos: {

      type: 'integer',

      default: 3,

    },

    playlistid: {

      type: 'string',

      default: '',

    },

    playlistviews:{

      type:"string",

      default:"grid"

    },

    channelviewmeta: {

      type: "boolean",

      default: false

    },

    publishviewmeta: {

      type: "boolean",

      default: false

    },

    title_view: {

      type: "boolean",

      default: true

    },

  },

  edit(props) {

    const { attributes, setAttributes } = props;

    const { playlistid, numbervideos, playlistviews,channelviewmeta, publishviewmeta,title_view } = attributes;



    const handleNumberChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ numbervideos: parseInt(newValue) });

    };



    const handleTextChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ playlistid: newValue });

    };



    const handleviewChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ playlistviews: newValue });

    };



    const handlechannelmetaChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ channelviewmeta: newValue });

    };

    

    const handlepublishmetaChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ publishviewmeta: newValue });

    };

    const handletitleviewChange = (newValue) => {

      tigger_feeder_editor();

      setAttributes({ title_view: newValue });

    };
    

    return (

      <div>

        <InspectorControls>

          <TextControl

            label="Enter PlayList ID"

            type="text"

            value={playlistid}

            onChange={handleTextChange}

          /> 

          <TextControl

            label="Number Of Videos"

            type="number"

            value={numbervideos}

            onChange={handleNumberChange}

          />

          <PanelBody title={"Playlist Style"} >

            <SelectControl

              label="Select Style"

              value={playlistviews}

              options={ [

                  { label: 'List', value: 'list' },

                  { label: 'Grid', value: 'grid' },

              ] }

              onChange={ handleviewChange }

            />

            <ToggleControl

            label="Title"

            checked={title_view}

            onChange={handletitleviewChange}

            />

          </PanelBody>

          <PanelBody title={"Video Meta"} >

            <ToggleControl

                label="Channel Name"

                checked={channelviewmeta}

                onChange={handlechannelmetaChange}

            />

            <ToggleControl

                label="Publish Date"

                checked={publishviewmeta}

                onChange={ handlepublishmetaChange }

            />

          </PanelBody>

        </InspectorControls>

        <ServerSideRender

          block="playbox-youtube/youtube-cta"

          attributes={{  numbervideos, playlistid, playlistviews, channelviewmeta, publishviewmeta, title_view }}

        />

      </div>

    );

  },

  save() {

    return null;

  },

});

