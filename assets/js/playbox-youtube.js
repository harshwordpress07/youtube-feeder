jQuery(function(){ 
    if( jQuery('body').hasClass('wp-admin') || jQuery('body').hasClass('elementor-editor-active') ){
        /* this for backend on load */
        window.tigger_feeder_editor = function(){
            setTimeout (function(){
                jQuery(".youtube-box-inner").magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false
                });
            },2000); 
        }
    }

    if( jQuery('body').hasClass('wp-admin') || jQuery('body').hasClass('elementor-editor-active')  ){
        /* this for backend on load */
           jQuery( window).on('load', tigger_feeder_editor() );
    }else{
        /* this for frontend on load */
        jQuery(".youtube-box-inner").magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }    
});    
