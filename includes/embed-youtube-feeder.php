<?php
/**
 * The embed of videos.
 * 
 * @package    Youtube_Feeder
 * @subpackage Youtube_Feeder/admin
 * @author     7Span
 * @copyright  2024 7Span
 * @link       https://#
 * @since      1.0.0
 **/

global  $video_items;   ?>
<section class="visit-playlist-section">
    <?php 
    $title_view = filter_var($args['title_view'], FILTER_VALIDATE_BOOLEAN);
    $publish_view_meta = filter_var($args['publish_view_meta'], FILTER_VALIDATE_BOOLEAN);
    $channel_view_meta = filter_var($args['channel_view_meta'], FILTER_VALIDATE_BOOLEAN);
    if( $title_view ): ?>
        <div class="visit-playlist-title">
            <h2><?php _e('Visit Our YouTube PlayList','youtube-feeder'); ?></h2>
        </div>
    <?php endif; ?>
    <!-- playlist Start -->
    <div class="visit-channel-wrap">
        <div class="visit-playlist-wrapper youtube-feeder-<?php echo $args['view_style']; ?>">
            <?php
            foreach( $video_items as $item ) {
                if( (isset($item['snippet']['resourceId']['videoId'])) OR (!empty($item['snippet']['resourceId']['videoId'])) ) { ?>
                    <div class="youtube-feeder-column">
                        <div class="youtube-box">
                            <a href="https://www.youtube.com/watch?v=<?php echo $item['snippet']['resourceId']['videoId'] ?>" class="youtube-box-inner">
                                <div class="img-wrap">
                                    <img src="http://img.youtube.com/vi/<?php echo $item['snippet']['resourceId']['videoId']."/mqdefault.jpg"?>" alt="">
                                </div>
                                <div class="youtube-text">
                                    <h4><?php echo $item['snippet']['title']; ?></h4>
                                    <?php if( $channel_view_meta || $publish_view_meta ): ?>
                                    <div class="youtube-meta">
                                        <?php if( $channel_view_meta ): ?>
                                        <div class="video-fedder-channel"><?php echo $item['snippet']['channelTitle']; ?></div>
                                        <?php endif;
                                        if( $publish_view_meta ): ?>
                                        <div class="video-fedder-publish"><?php echo video_feeder_time( $item['contentDetails']['videoPublishedAt'] ); ?></div>
                                        <?php endif; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php }
            } 
            ?>
        </div>
    </div>
    <!-- playlist End -->
</section>