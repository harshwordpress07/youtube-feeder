<?php
require_once PLAYBOX_YOUTUBE_PLUGIN_DIR . 'admin/elementor-widgets/class-playbox-youtube-elementor.php';
class PLAYBOX_YOUTUBE {  

    protected $plugin_name;
    protected $version;

    public function __construct() {
        if (defined('PLAYBOX_YOUTUBE_VERSION')) {
            $this->version = PLAYBOX_YOUTUBE_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'playbox-of-youtube-by-7span';

        add_action('admin_menu', [$this, 'playbox_youtube_listing_menus'], 99);
        add_action('admin_init', [$this, 'playbox_youtube_api_settings_init']);
        add_action('wp_enqueue_scripts', [$this, 'playbox_youtube_enqueue_script']);
        add_action('admin_notices', [$this, 'playbox_youtube_admin_notice']);
        add_action('enqueue_block_editor_assets', [$this, 'admin_playbox_youtube_enqueue_script']);
       // add_action('admin_init',  [$this,'check_and_load_elementor_widget']);
    }

    public function playbox_youtube_enqueue_script() {
        wp_register_script('magnific-popup-js', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/js/jquery.magnific-popup.js', array('jquery'), $this->version, true);
        wp_register_script('playbox-youtube-js', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/js/playbox-youtube.js', array('jquery'), $this->version, true);
        wp_register_style('front-playboxyoutube-css', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/css/playbox-youtube.css', array(), $this->version, 'all');
        wp_register_style('magnific-popup-css', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/css/magnific-popup.css', array(), $this->version, 'all');

        wp_enqueue_script('magnific-popup-js');
        wp_enqueue_script('playbox-youtube-js');
        wp_enqueue_style('front-playboxyoutube-css');
        wp_enqueue_style('magnific-popup-css');
    }

    public function admin_playbox_youtube_enqueue_script() {
        wp_register_script('admin-magnific-popup-js', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/js/jquery.magnific-popup.js', array('jquery'), $this->version, true);
        wp_register_script('admin-playbox-youtube-js', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/js/playbox-youtube.js', array('jquery'), $this->version, true);
        wp_register_script('custom-gutenburg-js', PLAYBOX_YOUTUBE_PLUGIN_URL . 'build/index.js', array('wp-blocks', 'wp-components', 'wp-element', 'wp-dom'), $this->version, true);

        wp_enqueue_script('admin-magnific-popup-js');
        wp_enqueue_script('admin-playbox-youtube-js');
        wp_enqueue_script('custom-gutenburg-js');
        wp_register_style('front-playboxyoutube-css', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/css/playbox-youtube.css', array(), $this->version, 'all');
        wp_register_style('magnific-popup-css', PLAYBOX_YOUTUBE_PLUGIN_URL . 'assets/css/magnific-popup.css', array(), $this->version, 'all');
        wp_enqueue_style('front-playboxyoutube-css');
        wp_enqueue_style('magnific-popup-css');
    }

    public function playbox_youtube_listing_menus() {
        $parent_slug = 'playbox-youtube-main';
        add_menu_page(
            __('YouTube Playbox Dashboard', 'playbox-of-youtube-by-7span'),
            __('YouTube Playbox', 'playbox-of-youtube-by-7span'),
            'administrator',
            $parent_slug,
            [$this, 'playbox_youtube_settings'],
            'dashicons-youtube'
        );
    }

    public function playbox_youtube_settings() {
        ?>
        <h1> <?php esc_html_e('YouTube Playbox Settings', 'playbox-of-youtube-by-7span'); ?> </h1>
        <form method="POST" action="options.php">
        <?php
            settings_fields('playbox_youtube_api_form_field');
            do_settings_sections('playbox_youtube_api_form_field');
            wp_nonce_field('playbox_youtube_save_api', 'playbox_youtube_nonce');
            submit_button();
        ?>
        </form>
        <?php
    }

    public function playbox_youtube_api_settings_init() {
        add_settings_section(
            'playbox_youtube_api_setting_section_id',
            '',
            [$this, 'playbox_youtube_api_add_setting_section'],
            'playbox_youtube_api_form_field'
        );

        add_settings_field(
            'playbox_youtube_api_setting_field',
            __('YouTube API key', 'playbox-of-youtube-by-7span'),
            [$this, 'playbox_youtube_api_markup_field'],
            'playbox_youtube_api_form_field',
            'playbox_youtube_api_setting_section_id'
        );
        register_setting('playbox_youtube_api_form_field', 'youtube_api_id', [$this, 'playbox_youtube_validate_api_id']);
    }

    function playbox_youtube_validate_api_id($input) {
        if (!isset($_POST['playbox_youtube_nonce']) || !wp_verify_nonce($_POST['playbox_youtube_nonce'], 'playbox_youtube_save_api')) {
            wp_die(esc_html__('Nonce verification failed', 'playbox-of-youtube-by-7span'));
        }
        return sanitize_text_field($input);
    }

    public function playbox_youtube_api_add_setting_section() {
    }

    public function playbox_youtube_api_markup_field() {
        $youtube_api_id = esc_attr(get_option('youtube_api_id'));
        ?>
        <input type="text" id="youtube_api_id" name="youtube_api_id" value="<?php echo esc_attr($youtube_api_id); ?>">
        <?php
    }

    public function playbox_youtube_admin_notice() {
        if (isset($_GET['settings-updated'])) {
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php esc_html_e('API key saved successfully.', 'playbox-of-youtube-by-7span'); ?></p>
            </div>
            <?php
        }
    }
}
