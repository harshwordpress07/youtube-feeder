<?php // phpcs:ignore

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    Youtube_Feeder
 * @subpackage Youtube_Feeder/admin
 * @author     7Span
 * @copyright  2024 7Span
 * @link       https://#
 * @since      1.0.0
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 */
//require_once YOUTUBE_FEEDER_PLUGIN_DIR . 'admin/sidebar-widgets/widget.php';
require_once YOUTUBE_FEEDER_PLUGIN_DIR . 'admin/elementor-widgets/class-youtube-feeder-elementor.php';

// phpcs:ignore
class YOUTUBE_FEEDER
{

    /**
     * The unique identifier of this plugin.
     *
     * @since  1.0.0
     * @access protected
     * @var    string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since  1.0.0
     * @access protected
     * @var    string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since 1.0.0
     */
    public function __construct()
    {

        if (defined('Youtube_Feeder_VERSION')) {
            $this->version = Youtube_Feeder_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'post-type-listing';

        add_action('admin_menu', [$this, 'youtube_feeder_listing_menus'], 99);

        add_action('admin_init', [$this, 'youtube_feeder_api_settings_init']);

        add_action('wp_enqueue_scripts', [$this, 'youtube_feeder_enqueue_script']);

        add_action('admin_notices', [$this, 'youtube_feeder_admin_notice']);

        add_action('enqueue_block_editor_assets', [$this, 'admin_youtube_feeder_enqueue_script']);

    }


    /**
     * Register the JavaScript for the user area.
     *
     * @since 1.0.0
     */
    public function youtube_feeder_enqueue_script()
    {



        wp_register_script('magnific-popup-js', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/js/jquery.magnific-popup.js', array('jquery'), $this->version, true);
        wp_register_script('youtube-feeder-js', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/js/youtube-feeder.js', array('jquery'), $this->version, true);
    

        wp_register_style('front-youtubefeeder-css', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/css/youtubefeeder.css', array(), $this->version, 'all');
        wp_register_style('magnific-popup-css', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/css/magnific-popup.css', array(), $this->version, 'all');

        wp_enqueue_script('magnific-popup-js');
        wp_enqueue_script('youtube-feeder-js');
    

        wp_enqueue_style('front-youtubefeeder-css');
        wp_enqueue_style('magnific-popup-css');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since 1.0.0
     */
    public function admin_youtube_feeder_enqueue_script()
    {

        wp_register_script('admin-magnific-popup-js', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/js/jquery.magnific-popup.js', array('jquery'), $this->version, true);
        wp_register_script('admin-youtube-feeder-js', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/js/youtube-feeder.js', array('jquery'), $this->version, true);
        

        wp_enqueue_script('admin-magnific-popup-js');
        wp_enqueue_script('admin-youtube-feeder-js');
            

        wp_register_script(
            'custom-gutenburg-js',
            YOUTUBE_FEEDER_PLUGIN_URL . 'build/index.js',
            array('wp-blocks', 'wp-components','wp-element','wp-dom'),
            $this->version,
            true
        );

        wp_enqueue_script('custom-gutenburg-js');

        wp_register_style('front-youtubefeeder-css', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/css/youtubefeeder.css', array(), $this->version, 'all');
        wp_register_style('magnific-popup-css', YOUTUBE_FEEDER_PLUGIN_URL . 'assets/css/magnific-popup.css', array(), $this->version, 'all');
        wp_enqueue_style('front-youtubefeeder-css');
        wp_enqueue_style('magnific-popup-css');
    }

    /**
     * Intilize the admin menu.
     *
     * @since 1.0.0
     */
    public function youtube_feeder_listing_menus()
    {

        $parent_slug = 'youtube-feeder-main';

        add_menu_page(__('YouTube Feeder Dasboard', $this->plugin_name), __('YouTube Feeder', $this->plugin_name), 'administrator', $parent_slug, [$this, 'youtube_feeder_settings'], 'dashicons-youtube');
    }

    /**
     * Display our primary menu page.
     *
     * @since 1.0.0
     *
     * @internal
     */
    public function youtube_feeder_settings()
    {
        ?>
        <h1> <?php esc_html_e('YouTube Feerder Setting', 'youtube-feeder'); ?> </h1>
        <form method="POST" action="options.php">
        <?php
        settings_fields('youtube_feeder_api_form_field');
        do_settings_sections('youtube_feeder_api_form_field');
        submit_button();
        ?>
        </form>

        <?php
    }

    /**
     * Display our API field setting initalize.
     *
     * @since 1.0.0
     *
     * @internal
     */
    public function youtube_feeder_api_settings_init()
    {

        add_settings_section(
            'youtube_feeder_api_setting_section_id',
            '',
            [$this, 'youtube_feeder_api_add_setting_section'],
            'youtube_feeder_api_form_field'
        );

        add_settings_field(
            'youtube_feeder_api_setting_field',
            __('Yotube API', 'youtube-feeder'),
            [$this, 'youtube_feeder_api_markup_field'],
            'youtube_feeder_api_form_field',
            'youtube_feeder_api_setting_section_id',
        );
        register_setting('youtube_feeder_api_form_field', 'youtube_api_id');
    }

    /**
     * Display our API field section.
     *
     * @since 1.0.0
     *
     * @internal
     */
    public function youtube_feeder_api_add_setting_section()
    {
    }

    /**
     * Display our API input field.
     *
     * @since 1.0.0
     *
     * @internal
     */
    public function youtube_feeder_api_markup_field()
    {
        ?>
        <input type="text" id="youtube_api_id" name="youtube_api_id" value="<?php echo get_option('youtube_api_id'); ?>">
        <?php
    }

    public function youtube_feeder_admin_notice() {
        if ( isset( $_GET['settings-updated'] ) ) {
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php esc_html_e('API saved successfully.', 'youtube-feeder'); ?></p>
            </div>
            <?php
        }
    }
}
