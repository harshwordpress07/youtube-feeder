/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ (function(module) {

module.exports = window["wp"]["element"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

const {
  registerBlockType
} = wp.blocks;
const {
  RichText,
  InspectorControls,
  ServerSideRender
} = wp.editor;
const {
  PanelBody,
  TextControl,
  SelectControl,
  ToggleControl
} = wp.components;
registerBlockType('playbox-youtube/youtube-cta', {
  title: 'YouTube Playbox',
  icon: 'video-alt3',
  category: 'common',
  attributes: {
    numbervideos: {
      type: 'integer',
      default: 3
    },
    playlistid: {
      type: 'string',
      default: ''
    },
    playlistviews: {
      type: "string",
      default: "grid"
    },
    channelviewmeta: {
      type: "boolean",
      default: false
    },
    publishviewmeta: {
      type: "boolean",
      default: false
    },
    title_view: {
      type: "boolean",
      default: true
    }
  },
  edit(props) {
    const {
      attributes,
      setAttributes
    } = props;
    const {
      playlistid,
      numbervideos,
      playlistviews,
      channelviewmeta,
      publishviewmeta,
      title_view
    } = attributes;
    const handleNumberChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        numbervideos: parseInt(newValue)
      });
    };
    const handleTextChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        playlistid: newValue
      });
    };
    const handleviewChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        playlistviews: newValue
      });
    };
    const handlechannelmetaChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        channelviewmeta: newValue
      });
    };
    const handlepublishmetaChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        publishviewmeta: newValue
      });
    };
    const handletitleviewChange = newValue => {
      tigger_feeder_editor();
      setAttributes({
        title_view: newValue
      });
    };
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(TextControl, {
      label: "Enter PlayList ID",
      type: "text",
      value: playlistid,
      onChange: handleTextChange
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(TextControl, {
      label: "Number Of Videos",
      type: "number",
      value: numbervideos,
      onChange: handleNumberChange
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
      title: "Playlist Style"
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(SelectControl, {
      label: "Select Style",
      value: playlistviews,
      options: [{
        label: 'List',
        value: 'list'
      }, {
        label: 'Grid',
        value: 'grid'
      }],
      onChange: handleviewChange
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ToggleControl, {
      label: "Title",
      checked: title_view,
      onChange: handletitleviewChange
    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PanelBody, {
      title: "Video Meta"
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ToggleControl, {
      label: "Channel Name",
      checked: channelviewmeta,
      onChange: handlechannelmetaChange
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ToggleControl, {
      label: "Publish Date",
      checked: publishviewmeta,
      onChange: handlepublishmetaChange
    }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ServerSideRender, {
      block: "playbox-youtube/youtube-cta",
      attributes: {
        numbervideos,
        playlistid,
        playlistviews,
        channelviewmeta,
        publishviewmeta,
        title_view
      }
    }));
  },
  save() {
    return null;
  }
});
}();
/******/ })()
;
//# sourceMappingURL=index.js.map